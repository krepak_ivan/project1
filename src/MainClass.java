import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) throws UnsupportedEncodingException {
        //Task 1 Java
        /*
        Scanner input = new Scanner(System.in);
        int size;
        size = input.nextInt();
        int myArray[] = new int[size];

        for (int i = 0; i < size; i++) {
            myArray[i] = input.nextInt();
        }

        int temp;
        for(int i = 0; i<size; i++){
            for(int j = i+1; j<size; j++){
                if (myArray[i] > myArray[j]){
                    temp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = temp;
                }
            }
        }

        System.out.println(myArray[0]);
         */

        //Task 2 Java
        /*
        int size;
        Scanner input = new Scanner(System.in);
        size = input.nextInt();
        int myArray[] = new int[size];
        double sumArray = 0;

        for(int i = 0; i < size; i++){
            myArray[i] = input.nextInt();
            sumArray = sumArray + myArray[i];
        }

        double avgArray;
        avgArray = sumArray / size;

        System.out.println(avgArray);
         */

        //Task 3
        /*
        int size;
        Scanner input = new Scanner(System.in);
        int n, i;
        int m = 0, flag = 0;

        n = input.nextInt();
        m = n / 2;

        for(i = 2; i<=m; i++){
            if(n%i == 0){
                System.out.println("Composite");
                flag = 1;
                break;
            }
            if(flag == 0){
                System.out.println("Prime");
                break;
            }
        }
         */

        //Task 4
        /*
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        int prod = 1;

        for(int i = n; i>=1; i--){
            prod = prod * i;
        }

        System.out.println(prod);
         */

        //Task 5
        //Class Main for task 5
        /*
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        System.out.println(fib(n));
         */

        //Task 6
        /*
        int a;
        int n;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        n = input.nextInt();

        System.out.println(Math.pow(a,n));
         */

        //Task 7
        /*
        Scanner input = new Scanner(System.in);
        int n;
        n = input.nextInt();
        int myArray[] = new int[n];

        for(int i = 0; i < n; i++){
            myArray[i] = input.nextInt();
        }

        reverse(myArray, 0, n-1);

        for(int i = 0; i < n; i++){
            System.out.println(myArray[i] + " ");
        }
         */

        //Task 8
        /*
        String theWord;
        Scanner input = new Scanner(System.in);
        theWord = input.nextLine();
        int lineSize;
        lineSize = theWord.length();
        int counterNumber = 0;

        byte[] byteArray = theWord.getBytes("US-ASCII");

        for(int i = 0; i<lineSize; i++){
            if((byteArray[i]>=65 && byteArray[i]<=90) || (byteArray[i]>=97 && byteArray[i]<=122)){
                counterNumber = counterNumber + 1;
            }
        }
        if(counterNumber == 0){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
         */

        //Task 9
        /*
        int k;
        Scanner input = new Scanner(System.in);
        k = input.nextInt();
        int n;
        n = input.nextInt();

        int upValue = 1;

        for(int i = k; i>=1; i--){
            upValue = upValue * i;
        }

        int downValue;
        int downValueA = 1;
        int downValueB = 1;

        for(int i = n; i>=1; i--){
            downValueA = downValueA * i;
        }

        for(int i = k - n; i>=1; i--){
            downValueB = downValueB * i;
        }

        downValue = downValueA * downValueB;

        int totalValue;
        totalValue = upValue / downValue;

        System.out.println(totalValue);
         */

        //Task 10 without recursion
        /*
        int a;
        int b;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        int gcd;

        if(a>b){
            a = a;
            b = b;
        }

        if(a<b){
            int temp;
            temp = a;
            a = b;
            b = temp;
        }

        if(a == b){
            gcd = a;
        }

        int c;
        c = a % b;

        if(b%c == 0){
            gcd = c;
        }else{
            gcd = 1;
        }

        System.out.println(gcd);
         */

        //Task 10 with recursion
        /*
        int a;
        int b;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        int g;
        g = gcd(a,b);

        System.out.println(g);
         */

        //Bonus problem 1
        /*
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        int myArray[] = new int[n];

        for(int i = 0; i<n; i++){
            myArray[i] = input.nextInt();
        }

        for(int i = 0; i<n-1; i++){
            for(int j = 0; j<n-i-1; j++){
                if(myArray[j]>myArray[j+1]){
                int temp = myArray[j];
                myArray[j] = myArray[j+1];
                myArray[j+1] = temp;
                }
            }
        }

        int counterArray;
        if(n == 0){
            counterArray = 0;
        }else{
            counterArray = 1;
        }

        for(int i = 0; i<n-1; i++){
                if(myArray[i] != myArray[i+1]){
                    counterArray = counterArray + 1;
                }

        }

        System.out.println(counterArray);
         */

        //Bonus problem 2
        /*
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        for(int i = 1; i<=n; i++){
            if(n == i){
                for(int j = 0; j < n; j++){
                    System.out.print(n/i + " ");
                }
                break;
            }
            System.out.println(n-i + " " + i);
        }
         */

        //Bonus problem 3
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        moveFunction(n, 1, 2, 3);

    //Reverse class for Task 7
    /*
    public static void reverse(int myArray[], int start, int end){
        int temp;
        if(start>=end) return;

        temp = myArray[start];
        myArray[start] = myArray[end];
        myArray[end] = temp;

        reverse(myArray, start+1, end-1);
    }
     */
    //Fib class for Task 5
    /*
    static int fib(int n){
        if(n<=1){
            return n;
        }
        return fib(n-1) + fib(n-2);
    }
     */
}
//Function for Task 10 with recursion
    /*
public static int gcd(int a, int b){
        if(a == 0) return b;
        return gcd(b%a, a);
}
     */

    public static void moveFunction(int disks, int a, int b, int c){
        if(disks > 0){
            moveFunction(disks - 1, a, c, b);
            System.out.println(disks + "" + a + "" + c);
            moveFunction(disks - 1, b, a, c);
        }
    }
}